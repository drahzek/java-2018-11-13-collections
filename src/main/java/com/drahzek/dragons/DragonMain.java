package com.drahzek.dragons;

import com.drahzek.dragons.model.Colour;
import com.drahzek.dragons.model.DragonCatalogue;

/**
 * Author: Dominik Swierzynski
 * Main class of an application responsible for the gathering, filtering and breeding of the dragons
 * This class is mostly responsible for executing (printing out) the methods defined in the dragon catalogue
 */
public class DragonMain {
    public static void main(String[] args) {

        DragonCatalogue dragonCatalogue = new DragonCatalogue();
        dragonCatalogue.readDragons("DragonsField.txt");
        dragonCatalogue.printAllDragons();
        dragonCatalogue.printAllNames();
        dragonCatalogue.printAllNamesColours();

        System.out.println("Najstarszy smok to: " + dragonCatalogue.getOldestOne());
        System.out.println("Największe skrzydła to : " + dragonCatalogue.getBiggestWingSpan());
        System.out.println("Najdłuższe imię ma znaków: " + dragonCatalogue.getNumberOfCharsLongestName());

        System.out.println(dragonCatalogue.listOfColouredDragons(Colour.Blue));
        System.out.println(dragonCatalogue.listOfNames());
        System.out.println(dragonCatalogue.listOfColours());

        System.out.println(dragonCatalogue.sortedNaturalOrder());
        System.out.println(dragonCatalogue.sortedByAge());

        System.out.println(dragonCatalogue.sumChecker(8000)); //50512 is the sum for the default dragons
        System.out.println(dragonCatalogue.colourChecker(Colour.Black)); //No default dragon is white, you can use it to check if it's working

        System.out.println("\nLista jaj\n" + dragonCatalogue.listOfAllEggs());
        System.out.println("\nLista jaj po eugenice\n" + dragonCatalogue.listOfUberDragonEggs(22));
    }
}
