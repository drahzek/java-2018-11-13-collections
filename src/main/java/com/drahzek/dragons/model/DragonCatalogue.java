package com.drahzek.dragons.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class DragonCatalogue {
    /**
     * Lists of dragons and their eggs, ready to be used
     */
    private List<Dragon> dragons = new ArrayList<>();
    private List<Dragon> eggs = new ArrayList<>();

    /**
     * method for adding the dragon to the list
     * @param dragon
     */
    public void addDragon (Dragon dragon) {
        dragons.add(dragon);
    }

    /**
     * method for removing the dragon from the list
     * @param dragon
     */
    public void removeDragon (Dragon dragon) {
        dragons.remove(dragon);
    }

    /**
     * method responsible for reading and adding the dragons from the prepared text file
     * to the dragons list for future use
     * @param file
     */
    public void readDragons (String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            while (br.ready()) {
                Colour colour = Colour.valueOf(br.readLine());
                int age = Integer.parseInt(br.readLine());
                int wingSpan = Integer.parseInt(br.readLine());
                String name = br.readLine();

                Dragon dragon = new Dragon(colour, age, wingSpan, name);
                addDragon(dragon);
            }

        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * method for printing all the dragons from the list with all the informations in the proper format
     */
    public void printAllDragons() {
        dragons.forEach(System.out::println);
    }

    /**
     * method for printing all of the dragons name
     */
    public void printAllNames() {
        System.out.println(dragons.stream()
                .map(Dragon::getName)
                .collect(toList()));
        //A different way to do the above
        //dragons.stream().map(Dragon::getName).forEach(System.out::println);
    }

    /**
     * method for printing all of the dragons name and their colours
     */
    public void printAllNamesColours() {
        dragons.stream()
                .sorted((d1, d2) -> d1.getName().compareTo(d2.getName()))
                .forEach(n -> System.out.println(n.getName() + " " + n.getColour()));
    }

    /**
     * method for getting the oldest dragon from the list
     * @return
     */
    public Dragon getOldestOne() {
        return dragons.stream()
                .max((s1, s2) -> Integer.compare(s1.getAge(), s2.getAge()))
                .orElse(null);
    }

    /**
     * method for getting the biggest wing span (not the dragon!) from the list
     * @return
     */
    public int getBiggestWingSpan() {
        return dragons.stream()
                .mapToInt(Dragon::getWingSpan)
                .max()
                .getAsInt();
    }

    /**
     * method for getting the number of letters in the longest name from the list
     * @return
     */
    public int getNumberOfCharsLongestName() {
        return dragons.stream()
                .map(Dragon::getName)
                .mapToInt(String::length)
                .max()
                .getAsInt();
    }

    /**
     * method for getting the list of dragons of the colour specified in the parameter
     * @param colour
     * @return
     */
    public List<Dragon> listOfColouredDragons (Colour colour) {
        return dragons.stream()
                .filter(p -> p.getColour()
                .equals(colour))
                .collect(toList());
    }

    /**
     * method for getting the list of names of the dragons on the existing list
     * @return
     */
    public List<String> listOfNames () {
        return dragons.stream()
                .map(Dragon::getName)
                .collect(toList());
    }

    /**
     * method for getting the list of uppercased colours of the dragons on the list (not all might be used!)
     * @return
     */
    public List<String> listOfColours () {
        return dragons.stream()
                .map(p -> p.getColour().name().toUpperCase())
                //.map(Dragon::getColour) //a different way to get what we want but it's ugly
                //.map(Colour::name)
                //.distinct() //if we want unique used values
                .collect(toList());
    }

    /**
     * method for getting the list of dragons sorted by natural order
     * @return
     */
    public List<Dragon> sortedNaturalOrder () {
        return dragons.stream()
                .sorted()
                .collect(toList());
    }

    /**
     * method for getting the list of dragons sorted by age
     * @return
     */
    public List<Dragon> sortedByAge () {
        return dragons.stream()
                .sorted(Comparator.comparing(Dragon::getAge))
                .collect(toList());
    }

    /**
     * method for getting the boolean value of whether the sum of all the dragons age
     * is bigger than the value given in the parameter or not
     * @param value
     * @return
     */
    public boolean sumChecker (int value) {
        return dragons.stream()
                .mapToInt(Dragon::getAge).sum()>value;
    }

    /**
     * method for getting the boolean value of whether there are any dragons of a colour
     * given in the parameter
     * @param colour
     * @return
     */
    public boolean colourChecker (Colour colour) {
        return dragons.stream()
                .map(Dragon::getColour).anyMatch(c -> c.equals(colour));
    }

    /**
     * method for getting the list of eggs generated from each dragon on the existing list
     * @return
     */
    public List<Egg> listOfAllEggs() {
        return dragons.stream()
                .map(e -> new Egg(e))
                .collect(toList());
    }
    /**
     * method for getting the list of eggs generated from the dragons on the existing list
     * but only if the said dragon has the wing span bigger than the given value
     * @param value
     * @return
     */
    public List<Egg> listOfUberDragonEggs(int value) {
        return dragons.stream()
                .filter(dragon -> dragon.getWingSpan()>value)
                .map(e -> new Egg(e))
                .collect(toList());
    }
}
