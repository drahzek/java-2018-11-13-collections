package com.drahzek.dragons.model;

public class Egg {
    private Colour colour;

    /**
     * egg constructor
     * The colour is taken from the dragon parent.
     * @param dragon
     */
    public Egg(Dragon dragon) {
        this.colour = dragon.getColour();
    }

    public Colour getColour() {
        return colour;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }

    @Override
    public String toString() {
        return(colour + "");
    }

}
