package com.drahzek.dragons.model;

import java.util.Objects;

/**
 * class responsible for the creation of dragons
 */
public class Dragon implements Comparable<Dragon> {
    private Colour colour;
    private int age;
    private int wingSpan;
    private String name;

    /**
     * dragon constructor
     * @param colour
     * @param age
     * @param wingSpan
     * @param name
     */
    public Dragon(Colour colour, int age, int wingSpan, String name) {
        this.colour = colour;
        this.age = age;
        this.wingSpan = wingSpan;
        this.name = name;
    }

    public Colour getColour() {
        return colour;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWingSpan() {
        return wingSpan;
    }

    public void setWingSpan(int wingSpan) {
        this.wingSpan = wingSpan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * override needed for the proper formatting of a dragons in the console string output
     * @return
     */
    @Override
    public String toString() {
        return(colour + " ") + (age + " ") + (wingSpan + " ") + (name);
    }

    /**
     * override needed for the comparison of the dragons.
     * The suggested order is : colour  wing span age name
     * @param dragon
     * @return
     */
    @Override
    public int compareTo(Dragon dragon) {
        int ret = colour.compareTo(dragon.colour);
        if (ret == 0) {
            ret = wingSpan - dragon.wingSpan;
        }
        if (ret == 0) {
            ret = age - dragon.age;
        }
        if (ret == 0) {
            ret = name.compareToIgnoreCase(dragon.name);
        }
        return ret;
    }
}
