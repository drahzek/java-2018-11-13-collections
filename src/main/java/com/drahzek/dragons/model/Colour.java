package com.drahzek.dragons.model;

/**
 * List of available colours for the dragons
 */
public enum Colour {
    Blue,
    Green,
    Black,
    Red,
    White;
}
